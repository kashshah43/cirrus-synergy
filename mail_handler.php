<?PHP
if(isset($_POST['submit'])){
    if(!empty($_POST['website'])) die();
    $sender = $_POST['email'];
    $recipient = 'info@cirrussynergy.com';

    if(isset($_POST['g-recaptcha-response'])){
        $captcha=$_POST['g-recaptcha-response'];
    }
    if(!$captcha){
        $success = "Please check the the captcha form.";
        header("location: contact.html?captcha=false#contact");
        exit;
    }
    $secretKey = "6Le4aqoUAAAAACFEuthKqpWq1-BpFmHkJkE75m9A";
    $ip = $_SERVER['REMOTE_ADDR'];
    // post request to server
    $url = 'https://www.google.com/recaptcha/api/siteverify?secret=' . urlencode($secretKey) .  '&response=' . urlencode($captcha);
    $response = file_get_contents($url);
    $responseKeys = json_decode($response,true);
    // should return JSON with success as true

    $first_name = $_POST['firstname'];
    $last_name = $_POST['lastname'];
    $subject = $_POST['subject'];
    
    $message = $first_name . " " . $last_name . " wrote the following:" . "\n\n" . $_POST['message'] . "\n\n" . " Contact Number: " . $_POST['phonenumber'];
    //$headers = 'From:' . $sender;
    $headers = array("From:" .$sender,
        "Reply-To:" .$sender,
        // "X-Mailer: PHP/" . PHP_VERSION
        "X-Mailer: PHP/" . phpversion(),
    );
    $headers = implode("\r\n", $headers);

    if ($responseKeys["success"] && mail($recipient, $subject, $message, $headers))
    {
        $success = "Message accepted";
        header("location: contact.html?success=true");
    }
    else
    {
        $success = "Message rejected";
        header("location: contact.html?error=false");
    }
}
?>
